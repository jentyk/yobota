from io import StringIO
import sys
from unittest import mock

from connectz import main, check_game, ReturnCode


@mock.patch.object(sys, 'argv', ['connectz'])
def test_no_input(capsys):
    main()
    assert capsys.readouterr().out == 'connectz.py: Provide one input file\n'


@mock.patch.object(sys, 'argv', ['connectz', 'non_such_a_file'])
def test_oserror(capsys):
    main()
    assert capsys.readouterr().out == f'{ReturnCode.FILE_ERROR}\n'


@mock.patch.object(sys, 'argv', ['connectz', 'invalid_file.txt'])
def test_invalid_file(capsys):
    main()
    assert capsys.readouterr().out == f'{ReturnCode.INVALID_FILE}\n'


@mock.patch.object(sys, 'argv', ['connectz', 'illegal_continue.txt'])
def test_illegal_continue(capsys):
    main()
    assert capsys.readouterr().out == f'{ReturnCode.ILLEGAL_CONTINUE}\n'


class TestCheckGame:
    def test_draw(self):
        assert (
            check_game(
                StringIO(
                    """3 3 3
1
2
3
1
3
2
1
3
2
"""
                )
            )
            == ReturnCode.DRAW
        )

    def test_win_for_player_1(self):
        assert (
            check_game(
                StringIO(
                    """3 3 3
1
2
1
2
1
"""
                )
            )
            == ReturnCode.PLAYER_1
        )

    def test_win_for_player_2(self):
        assert (
            check_game(
                StringIO(
                    """3 3 3
1
2
3
2
1
2
"""
                )
            )
            == ReturnCode.PLAYER_2
        )

    def test_win_for_player_1_horizontal(self):
        assert (
            check_game(
                StringIO(
                    """3 3 3
1
1
2
2
3
3
"""
                )
            )
            == ReturnCode.PLAYER_1
        )

    def test_win_for_player_1_diagonal(self):
        assert (
            check_game(
                StringIO(
                    """6 3 3
4
5
5
6
5
6
6
"""
                )
            )
            == ReturnCode.PLAYER_1
        )

    def test_win_for_player_1_bslash(self):
        assert (
            check_game(
                StringIO(
                    """6 3 3
6
5
5
4
6
4
4
"""
                )
            )
            == ReturnCode.PLAYER_1
        )

    def test_win_for_player_1_corner(self):
        assert (
            check_game(
                StringIO(
                    """6 6 2
5
6
6
"""
                )
            )
            == ReturnCode.PLAYER_1
        )

    def test_incomplete(self):
        assert (
            check_game(
                StringIO(
                    """3 3 3
1
2
3
1
"""
                )
            )
            == ReturnCode.INCOMPLETE
        )

    def test_illegal_row(self):
        assert (
            check_game(
                StringIO(
                    """3 3 3
1
2
2
1
1
2
2
"""
                )
            )
            == ReturnCode.ILLEGAL_ROW
        )

    def test_illegal_column(self):
        assert (
            check_game(
                StringIO(
                    """3 3 3
1
2
3
4
"""
                )
            )
            == ReturnCode.ILLEGAL_COLUMN
        )

    def test_illegal_game(self):
        assert (
            check_game(
                StringIO(
                    """3 3 4
1
2
3
1
3
2
1
3
2
2
"""
                )
            )
            == ReturnCode.ILLEGAL_GAME
        )
