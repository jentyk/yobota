from collections import defaultdict
from enum import IntEnum
import sys
from typing import TextIO, Dict


class PlayerOneWon(Exception):
    pass


class PlayerTwoWon(Exception):
    pass


class ReturnCode(IntEnum):
    DRAW = 0
    PLAYER_1 = 1
    PLAYER_2 = 2
    INCOMPLETE = 3
    ILLEGAL_CONTINUE = 4
    ILLEGAL_ROW = 5
    ILLEGAL_COLUMN = 6
    ILLEGAL_GAME = 7
    INVALID_FILE = 8
    FILE_ERROR = 9


def check_pattern(pattern: bytearray, player_1_win_pat: bytes, player_2_win_pat: bytes) -> None:
    if player_1_win_pat in pattern:
        raise PlayerOneWon
    if player_2_win_pat in pattern:
        raise PlayerTwoWon


def check_diagonals(
    game_rows: Dict[int, bytearray], player_1_win_pat: bytes, player_2_win_pat: bytes
) -> None:
    slash = bytearray()
    bslash = bytearray()
    for col in range(len(game_rows[1])):
        try:
            for r in game_rows:
                col += 1
                slash += game_rows[r][col].to_bytes(1, 'little')
                bslash += game_rows[r][-col].to_bytes(1, 'little')
            check_pattern(slash, player_1_win_pat, player_2_win_pat)
            check_pattern(bslash, player_1_win_pat, player_2_win_pat)
        except IndexError:
            break


def check_game(input_fh: TextIO) -> ReturnCode:
    width, height, minimum_counters = (int(p) for p in input_fh.readline().split())
    if minimum_counters > width and minimum_counters > height:
        return ReturnCode.ILLEGAL_GAME
    game_cols: Dict[int, bytearray] = defaultdict(bytearray)
    game_rows: Dict[int, bytearray] = defaultdict(lambda: bytearray(width + 1))
    row = 0
    player_1_win_pat = minimum_counters * b'\x01'
    player_2_win_pat = minimum_counters * b'\x02'
    try:
        while column := input_fh.readline():
            row += 1
            column_no: int = int(column)
            if column_no > width:
                return ReturnCode.ILLEGAL_COLUMN
            game_cols[column_no] += b'\x01' if row % 2 else b'\x02'
            col_len = len(game_cols[column_no])
            if col_len > height:
                return ReturnCode.ILLEGAL_ROW
            check_pattern(game_cols[column_no], player_1_win_pat, player_2_win_pat)
            game_rows[col_len][column_no] = 1 if row % 2 else 2
            check_pattern(game_rows[col_len], player_1_win_pat, player_2_win_pat)
        check_diagonals(game_rows, player_1_win_pat, player_2_win_pat)
    except PlayerOneWon:
        return ReturnCode.PLAYER_1
    except PlayerTwoWon:
        return ReturnCode.PLAYER_2
    if len(game_cols) == width and len(game_rows) == height:
        return ReturnCode.DRAW
    return ReturnCode.INCOMPLETE


def main() -> None:
    if len(sys.argv) != 2:
        print('connectz.py: Provide one input file')
        return
    try:
        with open(sys.argv[1]) as fh:
            return_code = check_game(fh)
            if return_code in (ReturnCode.PLAYER_1, ReturnCode.PLAYER_2) and fh.readline():
                return print(ReturnCode.ILLEGAL_CONTINUE.value)
            print(return_code.value)
    except IOError:
        print(ReturnCode.FILE_ERROR.value)
    except ValueError:
        print(ReturnCode.INVALID_FILE.value)


if __name__ == '__main__':  # pragma: no cover
    main()
